<img src="./logo.png" alt="logo" height="1%" width="20%" />

#### Etica Digitale
### Riconquistare il tempo e i social: oltre le bolle e le gabbie dorate

---

## Economia dell'attenzione

----

## Design manipolativo
- feed infiniti
- notifiche
- bombardamento di nuovi contenuti

----

## Scandali recenti
- Cambridge Analytica
- Myanmar

---

## Detox digitale

----

### Consigli pratici 
- Monitorare il tempo passate sulle app
- Disabilitare le notifiche delle singole applicazioni
- Mettere dei timer alle app, per esempio timer di 1 ora per TikTok

----

### Buone abitudini
- Tenere in un'altra stanza quando bisogna stare concentratɜ
- Non usare il telefono 1 ora prima di andare a dormire
- Evitare pagine e influencer che fanno contenuti basati sull'**indignazione**

----


### Accorgimenti
- non risolve problemi che si risolvono in **psicoterapia**
- Non pensare in modo binario **connessione-disconnessione**
- non è **lineare**

---

## Fediverso

![Giardino](./Giardino.jpg)

---

### Grazie

<img src="./logo.png" alt="logo" height="15%" width="15%" />

Sito: [eticadigitale.org](https://eticadigitale.org)

Telegram: [@EticaDigitale](https://t.me/EticaDigitale)

Mastodon: [@EticaDigitale](https://mastodon.bida.im/@EticaDigitale)

