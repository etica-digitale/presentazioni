<img src="../img/logo.png" alt="logo" height="1%" width="20%" />

### Riconquistare il tempo e i social: oltre le bolle e le gabbie dorate


[slide.eticadigitale.org](https://slide.eticadigitale.org)

---

## Perché le Big Tech sono _problematiche_?

----

### Economia dell'attenzione e design manipolativo
- Feed infiniti
- Notifiche
- Bombardamento di nuovi contenuti, trend, informazioni

----

### Le (aziende dietro) le piattaforme non sono neutrali
- [Scandalo Cambridge Analytica-Facebook](https://en.wikipedia.org/wiki/Facebook–Cambridge_Analytica_data_scandal): propaganda politica pro-Trump e pro-Brexit con i dati personali dellɜ utenti
- [Myanmar](https://www.zdnet.com/article/meta-slapped-with-two-class-action-lawsuits-worth-in-excess-of-150b-for-its-role-in-rohingya-genocide/): causa a Meta da 150 miliardi per aver alimentato le violenze contro la minoranza Rohingya
- Conflitto ucraino-russo e israelo-palestinese: [due pesi e due misure sulla moderazione](https://theintercept.com/2022/08/27/facebook-instagram-meta-russia-ukraine-war/)

---

## Detox digitale

----

### Consigli pratici 
- Monitorare il tempo passato sulle app
- Disabilitare le notifiche delle singole applicazioni
- Mettere dei timer alle app, per esempio timer di 1 ora per TikTok

----

### Buone abitudini
- Tenere in un'altra stanza quando bisogna stare concentratɜ
- Non usare il telefono 1 ora prima di andare a dormire
- Evitare pagine e influencer che fanno contenuti basati sull'**indignazione**

----


### Accorgimenti
- Non risolve problemi che si risolvono in **psicoterapia**
- Tra **connessione** e **disconnessione** c'è di mezzo un intero spettro
- Non è necessariamente **lineare**

---

## Fediverso

![Giardino](../img/Giardino.jpg)

----

### Caratteristiche
- Piattaforme? Meglio i **protocolli**
- Decentralizzato, libero, federato
- Possibilità di progettare uno spazio digitale secondo i propri valori

----

### Strumenti utili
- Feed non infinito
- Filtri
- Liste
- Content Warning
- Sii il tuo algoritmo

Note: L'idea del fediverso è che non c'è un entità centralizzata che gestisce il social network, ma ci sono più istanze di un social network, tutte tra loro federate e quindi intercomunicanti. Il Fediverso non è una piattaforma, ma si basa su un protocollo e tutti i social che parlano questo protocollo possono comunicare tra loro, per esempio da Mastodon (simile a Twitter) si possono commentare i video su PeerTube (simile a Youtube).

----

### Organizzazione
- La **struttura** (più) orizzontale è necessaria ma non sufficiente
- Le **istanze** sono come dei giardini collettivi: vanno curate
- Alcune **corporazioni** si stanno interessando ai social federati (Mozilla, Bluesky)
- Lavori in corso
- Fediverso come fucina di pratiche di lotta contro le Big Tech
- Istanze come esperimento per riprendersi i mezzi di comunicazione

---

## Per approfondire

----

##### Cronofagia
<img src="../img/Cronofagia-cover.jpg" alt="Cronofagia " height="25%" width="35%" />

(di Davide Mazzocco)

----

##### Digital Detox - The Politics of Disconnetting
<img src="../img/Digital-Detox-cover.jpeg" alt="Digital Detox" height="20%" width="30%" />

(di Trine Syvertsen)

---

### Grazie

<img src="../img/logo.png" alt="logo" height="15%" width="15%" />

Sito: [eticadigitale.org](https://eticadigitale.org)

Telegram: [@EticaDigitale](https://t.me/EticaDigitale)

Mastodon: [@EticaDigitale](https://mastodon.bida.im/@EticaDigitale)

---

Prossimo intervento (14.15)

_Tecnologie alternative, network e cooperazione oltre le piattaforme: sperimentazioni concrete ed esempi pratici_
