<img src="./img/logo.png" alt="logo" height="1%" width="20%" />

### Guida per tuttɜ al Fediverso

_Etica Digitale_

---

### Protocollo
- _Insieme di regole_
- permette di stabilire un modo universale di comunicare

---

### Istanze

<img src="./img/Istanze.jpg" alt="istanze" height="100%" width="100%" />

<div style="text-align: center; font-size: 30%">(David Revoy)</div>
---

### Regole
- Ogni istanza ha le proprie
- Trasparenti
- Decise dal basso

---

### Federazione e defederazione
- Seguo un utente → l'istanza segue la sua istanza
- La mia istanza decide quali istanze seguire e quali bloccare
- posso bloccare solo i media, silenziare o bloccare completamente altre istanze.

---

### Esperienza Livello Segreto

Collettività pensata come un giardino virtuale da curare.

Varie attività:
  - lore
  - quest

---


### Grazie

<img src="./img/logo.png" alt="logo" height="15%" width="15%" />

Sito: [eticadigitale.org](https://eticadigitale.org)

Telegram: [@EticaDigitale](https://t.me/EticaDigitale)

Mastodon: [@EticaDigitale](https://mastodon.bida.im/@EticaDigitale)

